const driverMiddleware = (req, res, next) => {
    if (req.user.role.toUpperCase() !== 'DRIVER') {
        return res
            .status(400)
            .json({message: 'This part is available only for drivers'});
    }
    next();
};

module.exports = {driverMiddleware};

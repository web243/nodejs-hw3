const express = require('express');
const {authMiddleware} = require('../middlewares/auth_middleware');
const {checkIfEditingAllowed, checkIfCanDeleteProfile} =
    require('../utils/validation/permissions_check');
const {getUserById,
    deleteUserById,
    changePasswordForUser,
} = require('./../services/users_service');
const {asyncWrapper} = require('./../utils/async_wrapper');

// eslint-disable-next-line new-cap
const router = express.Router();

router.use(authMiddleware);

router.get('/me', asyncWrapper(async (req, res) => {
    const user = await getUserById(req.user._id);
    res.status(200).json({user});
}));

router.delete('/me', asyncWrapper(async (req, res) => {
    const {user} = req;
    console.log(user);
    await checkIfEditingAllowed(user);
    console.log('1');
    await checkIfCanDeleteProfile(user);
    console.log('2');
    await deleteUserById(user._id);
    res.status(200).json({message: 'Success'});
}));

router.patch('/me', asyncWrapper(async (req, res) => {
    const {user} = req;
    await checkIfEditingAllowed(user);
    await changePasswordForUser(user._id,
        req.body.oldPassword,
        req.body.newPassword);
    res.status(200).json({message: 'Success'});
}));

module.exports = {
    usersRouter: router,
};

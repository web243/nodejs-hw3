const express = require('express');
const {authMiddleware} = require('../middlewares/auth_middleware');
const {getAssignedTruck} = require('../services/trucks_service');
const {changeLoadStateForUser} = require('../services/loads_service');
const {updateTruckById} = require('../services/trucks_service');
const {findTruckForLoad} = require('../services/trucks_service');
const {switchActiveTruckStatus} = require('../services/trucks_service');
const {asyncWrapper} = require('./../utils/async_wrapper');
const {getUserIdFromReq,
    getUserRoleFromReq} = require('./../utils/request_parser');
const {getAllLoadsForUser,
    getAllLoadsCountForUser,
    addLoadToShipper,
    getLoadByIdForShipper,
    changeLoadStatusForShipper,
    deleteLoadByIdForShipper,
    updateLoadByIdForShipper,
    getAssignedLoadForDriver,
    goToNextLoadStateForDriver} = require('./../services/loads_service');
const {validateUserRole} = require('./../utils/validation/permissions_check');
const {postLoadForShipper, iterateToNextLoadState} = require('./../services/system_service');

// eslint-disable-next-line new-cap
const router = express.Router();

router.use(authMiddleware);

const getZeroIfUndefined = (possibleUndefined) => {
    return possibleUndefined ? possibleUndefined : 0;
};

router.get('/active', asyncWrapper(async (req, res) => {
    validateUserRole(req.user, 'DRIVER');
    const driverId = getUserIdFromReq(req);
    const load = await getAssignedLoadForDriver(driverId);
    res.status(200).json({load});
}));

router.get('/:id/shipping_info', asyncWrapper(async (req, res) => {
    const shipperId = getUserIdFromReq(req);
    const load = await getLoadByIdForShipper(req.params.id, shipperId);
    const truck = await getAssignedTruck(load.assigned_to);
    res.status(200).json({load, truck});
}));

router.post('/:id/post', asyncWrapper(async (req, res) => {
    validateUserRole(req.user, 'SHIPPER');
    const shipperId = getUserIdFromReq(req);
    const loadId = req.params.id;
    const wasLoadPosted = await postLoadForShipper(loadId, shipperId);
    const message = wasLoadPosted ? 'Load posted successfully' :
        'Load was not posted';
    res.status(200)
        .json({message, driver_found: wasLoadPosted});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const shipperId = getUserIdFromReq(req);
    const load = await getLoadByIdForShipper(req.params.id, shipperId);
    res.status(200).json({load});
}));

router.get('/', asyncWrapper(async (req, res) => {
    const {offset, limit, ...loadsParams} = req.query;
    const userId = getUserIdFromReq(req);
    const userRole = getUserRoleFromReq(req);
    const loads = await getAllLoadsForUser(Object.assign({
        userId, userRole,
        offset, limit,
    }, loadsParams));
    const loadsCount = await getAllLoadsCountForUser(
        {userId, userRole, loadsParams: loadsParams},
    );
    res.status(200).json({
        offset: getZeroIfUndefined(offset),
        limit: getZeroIfUndefined(limit),
        count: loadsCount,
        loads,
    });
}));

router.post('/', asyncWrapper(async (req, res) => {
    validateUserRole(req.user, 'SHIPPER');
    const data = req.body;
    const userId = getUserIdFromReq(req);
    await addLoadToShipper(userId, data);
    res.status(200).json({message: 'Load created successfully'});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    validateUserRole(req.user, 'SHIPPER');
    const data = req.body;
    const loadId = req.params.id;
    const userId = getUserIdFromReq(req);
    await updateLoadByIdForShipper(loadId, userId, data);
    res.status(200).json({message: 'Load details changed successfully'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    validateUserRole(req.user, 'SHIPPER');
    const loadId = req.params.id;
    const userId = getUserIdFromReq(req);
    await deleteLoadByIdForShipper(loadId, userId);
    res.status(200).json({message: 'Load deleted successfully'});
}));

router.patch('/active/state', asyncWrapper(async (req, res) => {
    validateUserRole(req.user, 'DRIVER');
    const driverId = getUserIdFromReq(req);
    const newState = await iterateToNextLoadState(driverId);
    res.status(200).json({message: `Load state changed to '${newState}'`});
}));

module.exports = {
    loadsRouter: router,
};

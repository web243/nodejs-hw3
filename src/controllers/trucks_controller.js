const express = require('express');
const {authMiddleware} = require('../middlewares/auth_middleware');
const {driverMiddleware} = require('../middlewares/driver_middleware');
const {asyncWrapper} = require('./../utils/async_wrapper');
const {getAllTrucksForUser,
    getTruckByIdForDriver,
    addTruckToDriver,
    updateTruckByIdForDriver,
    deleteTruckByIdForDriver,
    assignTruckToDriver,
    getAssignedTruck} = require('./../services/trucks_service');
const {getUserIdFromReq} = require('./../utils/request_parser');
const {checkIfEditingAllowed} =
    require('../utils/validation/permissions_check');
const {validateUserRole} = require('./../utils/validation/permissions_check');

// eslint-disable-next-line new-cap
const router = express.Router();

router.use(authMiddleware);
router.use(driverMiddleware);

router.get('/active', asyncWrapper(async (req, res) => {
    const driverId = getUserIdFromReq(req);
    const truck = await getAssignedTruck(driverId);
    res.status(200).json({truck});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const driverId = getUserIdFromReq(req);
    const truck = await getTruckByIdForDriver(req.params.id, driverId);
    res.status(200).json({truck});
}));

router.get('/', asyncWrapper(async (req, res) => {
    const driverId = getUserIdFromReq(req);
    const trucks = await getAllTrucksForUser(driverId);
    res.status(200).json({
        trucks,
    });
}));

router.post('/', asyncWrapper(async (req, res) => {
    const type = req.body.type;
    const driverId = getUserIdFromReq(req);
    await addTruckToDriver(driverId, {type});
    res.status(200).json({message: 'Truck created successfully'});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    await checkIfEditingAllowed(req.user);
    const type = req.body.type;
    const truckId = req.params.id;
    const driverId = getUserIdFromReq(req);
    await updateTruckByIdForDriver(truckId, driverId, {type});
    res.status(200).json({message: 'Truck details changed successfully'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    await checkIfEditingAllowed(req.user);
    const truckId = req.params.id;
    const driverId = getUserIdFromReq(req);
    await deleteTruckByIdForDriver(truckId, driverId);
    res.status(200).json({message: 'Truck deleted successfully'});
}));

router.post('/:id/assign', asyncWrapper(async (req, res) => {
    await checkIfEditingAllowed(req.user);
    const truckId = req.params.id;
    const driverId = getUserIdFromReq(req);
    await assignTruckToDriver(truckId, driverId);
    res.status(200).json({message: 'Truck assigned successfully'});
}));


module.exports = {
    truckRouter: router,
};

const express = require('express');
const {getChat} = require('../services/chat_service');
const {InvalidDataError} = require('../utils/errors');
const {authMiddleware} = require('../middlewares/auth_middleware');
const {asyncWrapper} = require('../utils/async_wrapper');

// eslint-disable-next-line new-cap
const router = express.Router();

router.use(authMiddleware);

router.post('/', asyncWrapper(async (req, res) => {
    const {participants} = req.body;
    if (!participants) {
        throw new InvalidDataError('No participants for char provided');
    }
    const chat = await getChat(participants);
    res.status(200).json({chat: chat ? chat.messages : []});
}));

module.exports = {
    chatsRouter: router,
};

class UberProjectError extends Error {
    constructor(message) {
        super(message);
        this.status = 500;
    }
}

class InvalidCredentialsError extends UberProjectError {
    constructor(message = 'Invalid credentials') {
        super(message);
        this.status = 400;
    }
}

class InvalidIdError extends UberProjectError {
    constructor(message = 'Invalid ID. Nothing found') {
        super(message);
        this.status = 400;
    }
}

class WrongUserRoleError extends UberProjectError {
    constructor(message = 'This function is unavailable for your user role') {
        super(message);
        this.status = 400;
    }
}

class ForbiddenOperationError extends UberProjectError {
    constructor(message = 'You cannot perform this operation at the moment') {
        super(message);
        this.status = 400;
    }
}

class InvalidDataError extends UberProjectError {
    constructor(message = 'Invalid data') {
        super(message);
        this.status = 400;
    }
}

module.exports = {
    UberProjectError,
    InvalidCredentialsError,
    InvalidIdError,
    ForbiddenOperationError,
    WrongUserRoleError,
    InvalidDataError,
};

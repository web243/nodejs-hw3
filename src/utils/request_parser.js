const getUserIdFromReq = (req) => {
    return req.user._id;
};

const getUserRoleFromReq = (req) => {
    return req.user.role;
};

module.exports = {getUserIdFromReq, getUserRoleFromReq};

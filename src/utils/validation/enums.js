const loadStatuses = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];
const loadStates = ['En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery'];
const truckTypes = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];
const truckStatuses = ['OL', 'IS'];
const userRoles = ['SHIPPER', 'DRIVER'];

module.exports = {
    loadStates,
    loadStatuses,
    truckStatuses,
    truckTypes,
    userRoles,
};

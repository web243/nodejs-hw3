const Joi = require('joi');
const {passwordSchema} = require('./password_schema');

const credentialsSchema = Joi.object({
    email: Joi.string()
        .email()
        .required(),
}).concat(passwordSchema);

module.exports = {credentialsSchema};

const Joi = require('joi');
const {userRoles} = require('../enums');
const {credentialsSchema} = require('./credentials_schema');

const registrationSchema = Joi.object({
    role: Joi.string()
        .required()
        .valid(...userRoles),
}).concat(credentialsSchema);

module.exports = {registrationSchema};

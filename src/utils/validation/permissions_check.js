const {WrongUserRoleError} = require('../errors');
const {ForbiddenOperationError} = require('../errors');
const {getAssignedTruck} = require('../../services/trucks_service');
const {getActiveLoadsForShipper} = require('../../services/loads_service');

const checkIfEditingAllowed = async (user) => {
    if (user.role === 'DRIVER') {
        const truck = await getAssignedTruck(user._id);
        if (truck && truck.status === 'OL') {
            throw new ForbiddenOperationError();
        }
    }
};

const checkIfCanDeleteProfile = async (user) => {
    if (user.role === 'DRIVER') {
        const truck = await getAssignedTruck(user._id);
        if (truck && truck.status === 'OL') {
            throw new
            ForbiddenOperationError('You cannot delete your profile ' +
                'if you have a truck on load');
        }
    } else if (user.role === 'SHIPPER') {
        const loads = await getActiveLoadsForShipper(user._id);
        if (loads && loads.length) {
            throw new
            ForbiddenOperationError('You cannot delete your profile ' +
                'if you have an active load');
        }
    }
};

const validateUserRole = (user, expectedRole) => {
    const {role} = user;
    if (role !== expectedRole.toUpperCase()) {
        throw new WrongUserRoleError();
    }
};

module.exports = {checkIfEditingAllowed,
    validateUserRole, checkIfCanDeleteProfile};

const {Chat} = require('../models/chat_model');

const initChat = async (participants) => {
    const chat = new Chat({participants});
    await chat.save();
    return chat;
};

const getChat = async (participants) => {
    console.log(participants);
    return Chat.findOne({$and:
            [{participants: participants[0]},
                {participants: participants[1]}]});
};

const saveMessageToChat = async ({participants, content, from}) => {
    console.log('Saving message: ', content);
    let chat = await getChat(participants);
    if (!chat) {
        chat = await initChat(participants);
    }
    chat.messages.push({content, from});
    await chat.save();
};

module.exports = {getChat, saveMessageToChat};

const {Load} = require('./../models/load_model');
const {loadStates} = require('./../utils/validation/enums');

const initLogs = (logs) => {
    if (!logs) {
        logs = [];
    }
};

const pushLog = (logs, log) => {
    initLogs(logs);
    logs.push(log);
};

const getUserIdParam = (userRole, userId) => {
    return userRole === 'DRIVER' ? {'assigned_to': userId} :
        {'created_by': userId};
};

const getNextState = (curState) => {
    const i = loadStates.findIndex((state) => state === curState);
    if (i < loadStates.length - 1) {
        return loadStates[i + 1];
    }
    return loadStates[i];
};

const getAllLoadsForUser = async (params) => {
    const {userId, userRole, offset, limit, ...loadParams} = params;
    const userIdParam = getUserIdParam(userRole, userId);
    const options = Object.assign(userIdParam, {limit,
        skip: offset}, loadParams);
    return Load.find(options);
};

const getAllLoadsCountForUser = async ({userId, userRole, loadsParams}) => {
    const userIdParam = getUserIdParam(userRole, userId);
    return Load.count(Object.assign(userIdParam, loadsParams));
};

const getLoadByIdForShipper = async (loadId, userId) => {
    return Load.findOne({_id: loadId, created_by: userId});
};

const addLoadToShipper = async (userId, data) => {
    const load = new Load(Object.assign({created_by: userId}, data));
    await load.save();
};

const updateLoadByIdForShipper = async (loadId, userId, data) => {
    return Load.updateOne({_id: loadId, created_by: userId}, data);
};

const deleteLoadByIdForShipper = async (loadId, userId) => {
    return Load.deleteOne({_id: loadId, created_by: userId});
};

const getAssignedLoadForDriver = async (userId) => {
    return Load.findOne({assigned_to: userId, status: 'ASSIGNED'});
};

const getActiveLoadsForShipper = async (userId) => {
    return Load.find(
        {$or: [{created_by: userId, status: 'POSTED'},
            {created_by: userId, status: 'ASSIGNED'}]});
};

const changeLoadStatusForShipper = async (loadId, userId, status) => {
    const load = await getLoadByIdForShipper(loadId, userId);
    load.status = status;
    pushLog(load.logs, {
        message: `Load status changed to ${status}`,
    });
    await load.save();
};

const changeLoadStateForUser = async (loadId, userId, state) => {
    const load = await getLoadByIdForShipper(loadId, userId);
    load.state = state;
    pushLog(load.logs, {
        message: `Load state changed to ${state}`,
    });
    await load.save();
};

const goToNextLoadStateForDriver = async (userId) => {
    const activeLoad = await getAssignedLoadForDriver(userId);
    const currentState = activeLoad.state;
    const ARRIVED = 'Arrived to delivery';
    if (currentState === ARRIVED) {
        return null;
    }
    const newState = getNextState(currentState);
    activeLoad.state = newState;
    if (newState === ARRIVED) {
        activeLoad.status = 'SHIPPED';
        activeLoad.assigned_to = null;
    }
    await activeLoad.save();
    return newState;
};

module.exports = {getAllLoadsForUser,
    getAllLoadsCountForUser,
    addLoadToShipper,
    getLoadByIdForShipper,
    changeLoadStatusForShipper,
    updateLoadByIdForShipper,
    getAssignedLoadForDriver,
    deleteLoadByIdForShipper,
    goToNextLoadStateForDriver,
    getActiveLoadsForShipper,
    changeLoadStateForUser};

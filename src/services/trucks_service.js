const {ForbiddenOperationError} = require('../utils/errors');
const {Truck, trucksInfo} = require('../models/truck_model');

const hasEnoughPayload = (truck, load) => {
    const truckPayload = trucksInfo[truck.type].payload;
    const loadPayload = load.payload;
    return truckPayload >= loadPayload;
};

const hasEnoughSpace = (truck, load) => {
    const loadDimensions = load.dimensions;
    const truckDimensions = trucksInfo[truck.type].dimensions;
    return truckDimensions && loadDimensions &&
        loadDimensions.width <= truckDimensions.width &&
        loadDimensions.length <= truckDimensions.length &&
        loadDimensions.height <= truckDimensions.height;
};

const checkIfNotAssigned = async (truckId, userId) => {
    const truck = await getTruckByIdForDriver(truckId, userId);
    if (truck.assigned_to === userId) {
        throw new ForbiddenOperationError();
    }
};

const assignTruckToDriver = async (truckId, userId) => {
    const truck = await getAssignedTruck(userId);
    if (truck ) {
        throw new ForbiddenOperationError('You already have an assigned truck');
    }
    await Truck.updateOne({_id: truckId, created_by: userId},
        {assigned_to: userId, status: 'IS'});
};

const updateActiveTruckForDriver = async (userId, data) => {
    await Truck.updateOne({status: 'OL', created_by: userId}, data);
};

const switchActiveTruckStatus = async (userId) => {
    await updateActiveTruckForDriver(userId, {status: 'IS'});
};

const findTruckForLoad = async (load) => {
    const trucks = await Truck.find({status: 'IS'});
    for (let i = 0; i < trucks.length; i += 1) {
        const truck = trucks[i];
        if (truck.assigned_to &&
            hasEnoughPayload(truck, load) &&
            hasEnoughSpace(truck, load)) {
            return truck;
        }
    }
    return null;
};

const getAllTrucksForUser = async (userId) => {
    return Truck.find({created_by: userId});
};

const getTruckByIdForDriver = async (truckId, driverId) => {
    return Truck.findOne({_id: truckId, created_by: driverId});
};

const getAssignedTruck = async (driverId) => {
    if (driverId === null) {
        return null;
    }
    return Truck.findOne({assigned_to: driverId});
};

const addTruckToDriver = async (userId, data) => {
    const truck = new Truck(Object.assign({created_by: userId}, data));
    await truck.save();
};

const updateTruckById = async (truckId, data) => {
    await Truck.findByIdAndUpdate(truckId, data);
};

const updateTruckByIdForDriver = async (truckId, userId, data) => {
    await checkIfNotAssigned(truckId, userId);
    await Truck.updateOne({_id: truckId, created_by: userId}, data);
};

const deleteTruckByIdForDriver = async (truckId, userId) => {
    await checkIfNotAssigned(truckId, userId);
    await Truck.deleteOne({_id: truckId, created_by: userId});
};

module.exports = {getAllTrucksForUser,
    getTruckByIdForDriver,
    addTruckToDriver,
    updateTruckByIdForDriver,
    deleteTruckByIdForDriver,
    assignTruckToDriver,
    findTruckForLoad,
    updateTruckById,
    getAssignedTruck,
    switchActiveTruckStatus};

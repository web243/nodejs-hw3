const {switchActiveTruckStatus} = require('./trucks_service');
const {goToNextLoadStateForDriver} = require('./loads_service');
const {findTruckForLoad, updateTruckById} = require('./trucks_service');
const {changeLoadStatusForShipper,
    getLoadByIdForShipper,
    changeLoadStateForUser,
    updateLoadByIdForShipper} = require('./loads_service');

const sendTruckOnLoad = async (truckId) => {
    await updateTruckById(truckId, {status: 'OL'});
};

const postLoad = async (loadId, shipperId) => {
    await changeLoadStatusForShipper(loadId, shipperId, 'POSTED');
};

const rollBackLoadStatus = async (loadId, shipperId) => {
    await changeLoadStatusForShipper(loadId, shipperId, 'NEW');
};

const assignLoad = async (loadId, shipperId, driverId) => {
    await changeLoadStatusForShipper(loadId, shipperId, 'ASSIGNED');
    await changeLoadStateForUser(loadId, shipperId, 'En route to Pick Up');
    await updateLoadByIdForShipper(loadId, shipperId,
        {assigned_to: driverId});
};

const postLoadForShipper = async (loadId, shipperId) => {
    await postLoad(loadId, shipperId);
    const load = await getLoadByIdForShipper(loadId, shipperId);
    const truck = await findTruckForLoad(load);
    if (truck) {
        await assignLoad(loadId, shipperId, truck.assigned_to);
        await sendTruckOnLoad(truck._id);
        return true;
    }
    await rollBackLoadStatus(loadId, shipperId);
    return false;
};

const iterateToNextLoadState = async (driverId) => {
    const newState = await goToNextLoadStateForDriver(driverId);
    if (newState === 'Arrived to delivery') {
        await switchActiveTruckStatus(driverId);
    }
    return newState;
};

module.exports = {postLoadForShipper, iterateToNextLoadState};

const {User} = require('../models/user_model');
const {InvalidCredentialsError} = require('../utils/errors');
const {checkIfPasswordsMatch,
    encryptPassword,
} = require('../utils/password_processing');
const {passwordSchema} = require('../utils/validation/auth/password_schema');

const validatePasswords = (...passwords) => {
    passwords.forEach((password) => async () => {
        try {
            await passwordSchema.validateAsync(password);
        } catch (e) {
            throw new InvalidCredentialsError();
        }
    });
};

const getUserById = async (id) => {
    return User.findOne({_id: id});
};

const getUserByEmail = async (email) => {
    const user = await User.findOne({email: email});
    if (!user) {
        throw new InvalidCredentialsError();
    }
    return user;
};

const deleteUserById = async (id) => {
    return User.deleteOne({_id: id});
};

const changePasswordForUser = async (id, oldPassword, newPassword) => {
    validatePasswords(oldPassword, newPassword);
    const user = await getUserById(id);
    await checkIfPasswordsMatch(oldPassword, user.password);
    const passwordEncrypted = await encryptPassword(newPassword);
    return User.updateOne({_id: id}, {password: passwordEncrypted});
};

module.exports = {
    getUserById,
    getUserByEmail,
    deleteUserById,
    changePasswordForUser,
};

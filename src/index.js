require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');
const http = require('http');
const {authRouter} = require('./controllers/auth_controller');
const {usersRouter} = require('./controllers/users_controller');
const {truckRouter} = require('./controllers/trucks_controller');
const {loadsRouter} = require('./controllers/loads_controller');
const {chatsRouter} = require('./controllers/chats_controller');
const {UberProjectError} = require('./utils/errors');
const {Server} = require('socket.io');
const {saveMessageToChat} = require('./services/chat_service');

// @TODO: clear DB

const app = express();
const PORT = process.env.PORT;
const DB_URI = process.env.DB_URI;
process.env.NODE_ENV = 'production';
const dbOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
};

app.use(morgan('short'));
app.use(cors()); // Enable All CORS Requests
app.use(express.json());

const server = http.createServer(app);

const io = new Server(server, {
    cors: {
        origin: 'http://localhost:3000',
    },
});

io.use((socket, next) => {
    const {userId} = socket.handshake.auth;
    if (!userId) {
        return next(new Error('Invalid user ID'));
    }
    socket.userId = userId;
    next();
});

io.on('connection', (socket) => {
    socket.join(socket.userId);

    socket.on('private message', async ({content, to}) => {
        const from = socket.userId;
        socket.to(to).to(from).emit('private message', {
            content,
            from,
            to,
        });
        await saveMessageToChat({
            participants: [to, from],
            content, from});
    });

    socket.on('disconnect', () => {
        console.log('User disconnected');
    });
});

app.use('/api/auth', authRouter);
// auth
app.use('/api/users', usersRouter);
app.use('/api/loads', loadsRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/chats', chatsRouter);

app.use((req, res, next) => {
    res.status(404).json({message: 'Not found'});
});

app.use((err, req, res, next) => {
    if (err instanceof UberProjectError) {
        return res.status(err.status).json({message: err.message});
    }
    res.status(500).json({message: err.message});
});

const start = async () => {
    try {
        await mongoose.connect(DB_URI, dbOptions);
        server.listen(PORT, () => {
            console.log(`Server is listening on port ${PORT}`);
        });
    } catch (e) {
        console.error(e);
    }
};

start();

module.exports = {app};

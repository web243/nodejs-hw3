const mongoose = require('mongoose');

const MessageSchema = new mongoose.Schema({
    content: {
        type: String,
        required: true,
    },
    from: {
        type: String,
        required: true,
    },
    time: {
        type: Date,
        default: Date.now(),
    },
});

const Chat = mongoose.model('Chat', {
    participants: {
        type: [String],
        required: true,
    },
    messages: {
        type: [MessageSchema],
    },
});

module.exports = {Chat};

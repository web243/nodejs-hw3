const mongoose = require('mongoose');
const {truckTypes, truckStatuses} = require('./../utils/validation/enums');

const Truck = mongoose.model('Truck', {
    created_by: {
        type: String,
        required: true,
    },
    assigned_to: {
        type: String,
        default: null,
    },
    type: {
        type: String,
        required: true,
        uppercase: true,
        enum: truckTypes,
    },
    status: {
        type: String,
        uppercase: true,
        default: 'IS',
        enum: truckStatuses,
    },
    created_date: {
        type: Date,
        default: Date.now(),
    },
});

const trucksInfo = {
    'SPRINTER': {
        dimensions: {
            width: 300,
            length: 250,
            height: 170,
        },
        payload: 1700,
    },
    'SMALL STRAIGHT': {
        dimensions: {
            width: 500,
            length: 250,
            height: 170,
        },
        payload: 2500,
    },
    'LARGE STRAIGHT': {
        dimensions: {
            width: 700,
            length: 350,
            height: 200,
        },
        payload: 4000,
    },
};

module.exports = {Truck, trucksInfo};

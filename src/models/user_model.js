const mongoose = require('mongoose');
const {userRoles} = require('./../utils/validation/enums');

const User = mongoose.model('User', {
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        required: true,
        uppercase: true,
        enum: userRoles,
    },
    created_date: {
        type: Date,
        default: Date.now(),
    },
});

module.exports = {User};

const mongoose = require('mongoose');
const {loadStates, loadStatuses} = require('./../utils/validation/enums');

const LogSchema = new mongoose.Schema({
    message: {
        type: String,
        required: true,
    },
    time: {
        type: Date,
        default: Date.now(),
    },
});

const DimensionsSchema = new mongoose.Schema({
    width: {
        type: Number,
        required: true,
    },
    length: {
        type: Number,
        required: true,
    },
    height: {
        type: Number,
        required: true,
    },
},
);

const Load = mongoose.model('Load', {
    created_by: {
        type: String,
        required: true,
    },
    assigned_to: {
        type: String,
        default: null,
    },
    status: {
        type: String,
        default: 'NEW',
        required: true,
        uppercase: true,
        enum: loadStatuses,
    },
    state: {
        type: String,
        enum: loadStates,
    },
    name: {
        type: String,
        required: true,
    },
    payload: {
        type: Number,
        required: true,
    },
    pickup_address: {
        type: String,
        required: true,
    },
    delivery_address: {
        type: String,
        required: true,
    },
    dimensions: {
        type: DimensionsSchema,
        required: true,
    },
    logs: {
        type: [LogSchema],
        of: String,
        default: [],
    },
    created_date: {
        type: Date,
        default: Date.now(),
    },
});

module.exports = {Load};

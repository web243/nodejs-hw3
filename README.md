# NodeJS HW3
## Description
An UBER-like service for freight trucks. 

Written in REST style, using NodeJS and MongoDB as a database. 

The service can help regular people to deliver their stuff and help drivers to find loads and earn some money. 

The application contains 2 roles, driver and shipper. JWT authentication is provided.
Besides standard CRUD functionality for loads and trucks, a user can change their profile info. Driver and shipper can communicate through a chat (written on sockets).

*Note*: you can find an API doc in the `openapi.yaml` file. 

## Installation
To start the server, run these commands from the project root:
```bash
npm install
npm start
```
Also, make sure the port 8080 is available.

## Usage
This project may be used as a part of some application. *Note*: if you search for the finished solution, the front-end is already provided. You can find the front-end project at [this repo](https://gitlab.com/web243/nodejs-hw3-ui). 

